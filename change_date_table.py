from itertools import cycle
from datetime import datetime, timedelta
from dateutil.rrule import (
    rruleset, rrule, MO, TU, WE, TH, FR, SA, SU, DAILY, WEEKLY, YEARLY)
from dateutil.relativedelta import relativedelta, MO as rdMO
change_passwords_on = MO
years = 2
start = datetime.now() + timedelta(7) # leave a week to make the notebook
categories = (                        # (name, period in weeks)
    (  'A',      9   ),
    (  'B',      18  ),
    (  'C/X',      36  ))
def week_of(day):
    """The 7 days starting the Monday before day"""
    return rrule(DAILY, day + relativedelta(day, weekday=rdMO(-1)), count=7)
dates = rruleset()
dates.rrule(rrule(WEEKLY, start, count=52*years,
                  byweekday=change_passwords_on))
thanksgivings = rrule(YEARLY, start, count=years, bymonth=11, byweekday=TH,
                      bysetpos=4)
for dt in thanksgivings: dates.exrule(week_of(dt))
christmases = rrule(YEARLY, start, count=years, bymonth=12, bymonthday=25)
for dt in christmases: dates.exrule(week_of(dt))
cycles = [cycle(range(1, period+1)) for name, period in categories] # 1-based
changes = zip(dates, *cycles)
print("   Date      " + "  ".join('{:^3s}'.format(n) for n,p in categories))
print("----------   " + "  ".join('---' for n,p in categories))
row_f = ("{}  " + "  ".join('{:3d}' for n,p in categories))
for row in changes:
    date = row[0]
    print(row_f.format(date.strftime('%Y-%m-%d'), *row[1:]))
