% \documentclass[ebook,12pt,oneside,openany]{memoir}
% \documentclass[letter]{article}
\documentclass{article}
\usepackage[paperwidth=4.25in,paperheight=5.5in]{geometry}

\usepackage{verbatim}
\usepackage[normalem]{ulem}
\usepackage{hyperref}
\hypersetup{
  colorlinks=true,
  bookmarks=true,
  bookmarksopen=true,
  bookmarksnumbered=true,
}

\title{A paper-based authentication policy}
\author{Jared Jennings}
\date{\today}

\newcommand{\thead}[1]{{\small \emph{#1}}}

\begin{document}
\maketitle
\tableofcontents
\listoftables

\section{Colophon}

This document is made available under the
\href{https://creativecommons.org/publicdomain/zero/1.0/legalcode}{CC0
  1.0 Universal license}. It is typeset using \LaTeX and rendered
into a booklet by \texttt{pdfjam}.

\section{Introduction}

This is how to keep a notebook of passwords you randomly generate,
mostly with dice.

Write them?---You want to pick secure passwords but you have lots of
accounts. Writing them down frees you from picking weak passwords or
using the same password for multiple accounts.

On paper?---Any networked device is at some risk of being subverted,
so only the passwords constantly used on a device should be stored
there. All of your passwords should not be stored on any networked
device, including the cloud.

With dice?---Secure passwords must be generated randomly; but
computers can be made to fail at randomness very subtly. In this
scheme, the most important passwords are generated off the computer,
by physically random processes.

Most places where you set up an account will not remind you to change
your password periodically, so your notebook will help you do
this. Some places have silly rules that make it harder to pick a
secure, simple password. Some passwords are more important than
others. These factors are taken into, eheh, \emph{account} by this
policy.


\section{Risk}

Before expending effort to secure your passwords, you need to know
where to focus your effort, because you have a finite amount of it.

Each account you have holds something of value to you: your banking
information, the ability to talk to others, photos of your
family. Attackers place these things at risk by attempting to
authenticate as you.

Rather than enumerate all the ways attackers can get your password,
let's just say they can get it within some time after you set it.  How
long depends on the value of the account to the attacker, the power of
the attacker, and the restrictions placed on the password by the
service or site (\emph{e.g.}, must be exactly eight characters). To
defend, you need to change the password before you think the attacker
can get it.

Attackers will also try going around the password prompt. To defend,
find out all the ways you can try to reset your password. Make note of
any questions asked (\emph{e.g.}, mother's maiden name). See if you
can make the answers harder (\emph{e.g.} make up a random answer
instead of using the real one; you'll need to write this down). Make
note of any other accounts used to open access back up (\emph{e.g.},
hyperlinks to password reset pages are often sent to your email
account). These accounts have greater value to an attacker.

Some sites let you routinely use your identity from another site to
log in. For example, Trello lets you log in with a Google account, or
Spotify lets you use your Facebook account to get in. Accounts that
open access to other accounts like this have greater value to an
attacker.

You should use two-factor authentication where possible. But, as
above, find out what process happens if a factor is missing, and think
about how an attacker could go through that process, and how you can
make it harder for them. Or, if the second factor involves one of your
accounts, that account has greater value to an attacker. (Remember
that your cell phone is an account.)





\section{Risk categorization}
\label{riskcategories}

In Table~\ref{riskcategorytable}, then, is a categorization of risk to
accounts. After thinking over the risks outlined above, you may need
to alter it to your needs.

\begin{table}
\centering
\begin{tabular}{p{0.6\textwidth} c l}
\hline
\thead{If an account ...} & & \thead{Category} \\
\hline
\hline
opens access to other accounts (e.g., email) & $\Rightarrow$ & A \\
can spend money & $\Rightarrow$ & A \\
is used every day & $\Rightarrow$ & A \\
controls words said in public & $\Rightarrow$ & A \\
is a privileged account on the network & $\Rightarrow$ & A \\
\hline
has password rules that prevent good passwords & $\Rightarrow$ & B \\
is used by a service or device not a person & $\Rightarrow$ & B \\
\hline
stands alone and has a decent password policy & $\Rightarrow$ & C \\
\hline
password is used in case of two-factor authentication failure & $\Rightarrow$ & X \\
\hline
\end{tabular}
\caption{Risk categories.}
\label{riskcategorytable}
\end{table}


Table~\ref{riskcategorydefinitiontable} shows what should be done
about the risk: how often passwords must be changed (in weeks), and
how long they should be.

% this gets used in the examples below; if you change the values in
% the table below, change this definition accordingly
\newcommand{\aweeks}{9}

\begin{table}
\centering
\begin{tabular}{l r r r r}
\hline
\thead{Category} & \thead{Weeks} & \thead{Entropy (bits)} & \thead{Words} & \thead{Chars} \\
\hline
\hline
A & 9 & 90 & 7 & 15 \\
B & 18 & 77 & 6 & 13 \\
C & 36 & 77 & 6 & 13 \\
X & 36 & 100 & 8 & 20 \\
Backups & never & 100 & 8 & 20 \\
\hline
\end{tabular}
\caption{Risk categories defined.}
\label{riskcategorydefinitiontable}
\end{table}


\section{Getting started}

Get a small notebook with about 40 pages, a pen, and five dice. Visit
\url{http://diceware.com/} and print out the word list. If you can
print it 6-up, even nicer.

Your notebook is the cornerstone to your password strategy, but not
its centerpiece. Make your devices remember the passwords they
constantly need. Each device should lock itself, and the password to
unlock it must be as strong as the most critical password it
remembers. The (paper) notebook will remember your infrequently used
passwords and those you need but forget. Put it in a safe when you're
not using it.

Before you go on a trip, think about what accounts you might need and
make sure your devices know the passwords. Don't take your notebook
with you.

You're going to decide on a risk category (\S\ref{riskcategories}) for
each of your accounts. Each risk category has a number of groups, and
every week you're going to change the passwords in one group of each
category. Depending on how many accounts you've got, and how risky
they are, this might be three or four passwords every week.


\section{What to write in the notebook}

\subsection{Change schedule}

Write a table like this in the front of the notebook. It will take
several pages. This table lets you know which passwords to change each
week.

\vspace{1em}

\begin{tabular}{l l l l}
\hline
Date & A & B & C/X \\
\hline
\hline
2016-01-29  &  1  &  1  &  1 \\  
2016-02-05  &  2  &  2  &  2 \\  
2016-02-12  &  3  &  3  &  3 \\  
2016-02-19  &  4  &  4  &  4 \\  
2016-02-26  &  5  &  5  &  5 \\  
2016-03-04  &  6  &  6  &  6 \\  
2016-03-11  &  7  &  7  &  7 \\  
2016-03-18  &  8  &  8  &  8 \\  
2016-03-25  &  9  &  9  &  9 \\  
2016-04-01  &  1  & 10  & 10 \\  
2016-04-08  &  2  & 11  & 11 \\  
2016-04-15  &  3  & 12  & 12 \\
2016-04-22  &  4  & 13  & 13 \\
2016-04-29  &  5  & 14  & 14 \\
2016-05-06  &  6  & 15  & 15 \\
2016-05-13  &  7  & 16  & 16 \\
2016-05-20  &  8  & 17  & 17 \\
2016-05-27  &  9  & 18  & 18 \\
2016-06-03  &  1  &  1  & 19 \\
2016-06-10  &  2  &  2  & 20 \\
2016-06-17  &  3  &  3  & 21 \\
2016-06-24  &  4  &  4  & 22 \\





\hline
\end{tabular}

\vspace{2em}

% https://www.quora.com/profile/Stefan-Bucur/Posts/How-To-Break-a-Long-Word-Without-a-Hyphen-in-LaTeX
You can use the included Python script 
({\tt change\_\allowbreak{}date\_\allowbreak{}table.py})
to emit the table, or you can write the table yourself by following
this pattern:

The leftmost column contains dates running every week. If there are
some weeks when you know in advance you won't have time to change
passwords, perhaps because there are holidays, do so.

Then there is a column for each risk category (see
Table~\ref{riskcategorytable}). The numbers down the column are a
repeating cycle of 1 through however many weeks is the maximum age of
a password in that category (see
Table~\ref{riskcategorydefinitiontable}). For example, category A
passwords must be changed every \aweeks\ weeks, so the numbers
1-\aweeks\ repeat down the A column.

If the table is slim enough, you can write it in two columns.

Write this out for a couple of years ahead. Start with the first day
when you plan to change passwords. On each of these dates, you're
going to change all the passwords in the written group of each
category. Your accounts will be evenly divided between the groups.

Set reminders for yourself to change the passwords on the dates you
have written.



\subsection{Account categorization and grouping}

Now, reserve a page or two for each account category. Write the
numbers 1 through \textsl{weeks}, leaving vertical space between
them. You're going to divide all of your accounts in that category
evenly over these \textsl{weeks}. For example, if you have 30
category-A passwords, write 1 through \aweeks , leaving four lines
under each number ($9 \times 4 = 36$, leaving you a bit of room to
grow). But if you have 26 category-C passwords, and 36 weeks in
category C, you only have to leave 1 line of space by each number
($36 \times 1 = 36$, leaving you ten blank weeks).

Deal the accounts to groups in a round-robin fashion. Write the name
of an account in week 1, then write the name of the next account in
week 2, \emph{etc.}, next account in week \aweeks , then go back to
week 1 and write the name of the next account there. It's not really
important here to keep the accounts sorted, merely categorized.

Let us suppose your 30 Category A accounts are at websites whose names
begin with successive letters of the alphabet, and your 26 Category C
accounts are all at French websites. You might write something like
this:

\vspace{2em}

\textbf{Category A}

% for i in a j s b b k t c c l u d d m v e n w f o x g p y h q x i r a; do shuf < /usr/share/dict/words | grep -m 1 ^$i; done | tr -d "'" | sed 's/$/.com/'
%
% the ordering was got by writing the letters by columns on some
% paper, and reading off by rows.
\begin{description}
\item[Group 1] amateur.com --- jollying.com --- screwed.com --- blonds.com
\item[Group 2] boundarys.com --- knees.com --- tissues.com --- curly.com
\item[Group 3] contacts.com --- lamb.com --- upholsterers.com --- dowry.com
\item[Group 4] depravitys.com --- moratoriums.com --- vehicular.com
\item[Group 5] earplugs.com --- necklace.com --- welters.com
\item[Group 6] floatations.com --- outperformed.com --- xenophobic.com
\item[Group 7] gesticulates.com --- pleasantrys.com --- yeshivas.com
\item[Group 8] horseradishes.com --- quoits.com --- xenophobic.com
\item[Group 9] imbalances.com --- release.com --- aortae.com

\end{description}

\vspace{2em}
\textbf{Category B}

[...]

\vspace{2em}
\textbf{Category C}

% for i in a b c d e f g h i j k l m n o p q r s t u v w x y z; do shuf < /usr/share/dict/french | grep -m 1 ^$i; done | tr -d "'" | sed 's/$/.com/'

\begin{description}
\item[Group 1] ambitionn\'ees.com
\item[Group 2] butor.com
\item[Group 3] centrerai.com
\item[Group 4] disciplinerez.com
\item[Group 5] emmagasinerons.com
\item[Group 6] faibli.com
\item[Group 7] gambader.com
\item[Group 8] haletiez.com
\item[Group 9] intenterai.com
\item[Group 10] jalous\'ees.com
\item[Group 11] kilom\'etriques.com
\item[Group 12] laveurs.com
\item[Group 13] mobili\`eres.com
\item[Group 14] narguerez.com
\item[Group 15] orientales.com
\item[Group 16] percevra.com
\item[Group 17] querell\'es.com
\item[Group 18] refusent.com
\item[Group 19] sportifs.com
\item[Group 20] traînerai.com
\item[Group 21] uniformis\'ees.com
\item[Group 22] vallonn\'e.com
\item[Group 23] wagon-poste.com
\item[Group 24] x\'er\`es.com
\item[Group 25] yacks.com
\item[Group 26] zappant.com
\item[Group 27]
\item[Group 28]
\item[Group 29]
\item[Group 30]
\item[Group 31]
\item[Group 32]
\item[Group 33]
\item[Group 34]
\item[Group 35]
\item[Group 36]
\end{description}

\vspace{2em}

\subsection{Accounts known by devices}

Take a half a page per device and write which passwords the device
knows. This way, if you lose it or it is compromised, you know which
passwords you need to change.

\subsection{Account passwords}

This is all the rest of the notebook. Write the alphabet on the outer
edges of the remaining pages: here the accounts are sorted because you
have to search for them. Leave room under each account for all the
passwords you said you'd write down in the tables earlier. For
example, if the account is in Category A, Group 3 (of 9), and you
intend your notebook to last for two years of password changes, find
all the 3's in the table at the front of the notebook, and you'll see
you need room for 11 passwords.

\vspace{1em}

\begin{tabular}{p{0.5\textwidth} l}
\hline
\textbf{example.org} & jdoe56 \\
\emph{Write any restrictions on the password, e.g.} No spaces \\
\hline
\end{tabular}

\vspace{1em}

Initial: flarblezart \\
correcthorsebatterystaple \\
hyphenwordscosponsoredrapidest \\
impugnedvowscrumbledCamemberts \\
spermicidesdrawbridgessonskits \\
...

\vspace{2em}


\subsection{Fake answers and auxiliary passwords}

Every time you make up an answer for a security question, or if there
is an extra password only used when your dongle is missing, or such,
write it on the page with the account it goes to.


\section{Conclusion}

It may be true that any way to authenticate yourself involves a
tradeoff between security, convenience and privacy. Passwords are not
great for convenience, but when managed as set out here they will
provide decent security, and they are excellent for privacy (a site
need not know anything about you to authenticate you, except the
password you set up).

Stay safe and have fun!

\end{document}
