# What is it?

This is [a document](password-policy.pdf) about how to keep a notebook
of passwords you randomly generate, mostly with dice, and why you
might do such a thing.

# How to build?

If you change this document, you'll need to build it into a pdf to see
it. (If you just want to see it in its present state, click on the
link in the section above.) You need LaTeX, make, Python and a
command-line sort of attitude.

To build the document, just type `make`. It will guide you in the
construction of a table, using a Python script, which you can run
by typing `python change_date_table.py`.

## If you don't have a command-line sort of attitude

If you're on Windows, get ProTeXt; open up the `.tex` file with
TeXStudio. Python for Windows is also available; use IDLE to open and
run the Python script.

If you're on a Mac, get MacTeX, and open up the `.tex` file with
TeXShop. You can use a Terminal to run `python change_date_table.py`.

# License and contributing

This document is available to you under the [CC0 1.0 Universal
license](https://creativecommons.org/publicdomain/zero/1.0/legalcode).

Please file issues, and fork and send pull requests, using GitLab.